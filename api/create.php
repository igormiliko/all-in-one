<?php

    if (isset($_POST['product_sku']) && isset($_POST['product_name']) && isset($_POST['product_price']) && isset($_POST['product_type']) && isset($_POST['product_property']))
    {

        include 'classes/Book.php';
        include 'classes/Dvd.php';
        include 'classes/Furniture.php';
        include 'increment-id.php';

        $product_type_to_eval = $_POST['product_type'];

        eval("\$product = new ".$product_type_to_eval."();");

        // Setting the data into the object class
        $product->set_ID ($id_to_insert); // this $id_to_insert are included in the increment_id.php file
        $product->set_sku ($_POST['product_sku']);
        $product->set_name ($_POST['product_name']);
        $product->set_price ($_POST['product_price']);
        $product->set_type ($_POST['product_type']);
        $product->set_property ($_POST['product_property']);
        
        if ($product->insert($product->get_ID(), $product->get_sku(),$product->get_name(), $product->get_price(),
        $product->get_type(), $product->get_property()))
        {
            $data = array('result' => 'success');
        }
        else
        {
            $data = array('result' => 'error');
        }

        echo json_encode($data);
    }
?>