<?php
    $id_to_insert;

    $books = new Book();
    $dvds = new Dvd();
    $furnitures = new Furniture();

    $all_products = array_merge($books->fetchAll(), $dvds->fetchAll(), $furnitures->fetchAll());
    $arr_length = count($all_products);

    if($arr_length == 0)
    {
        $id_to_insert = 1;
    } 
    else
    {   

        // I'm sorting the array to continually implement the ID
        // Because the ID must be unique and have a continuous 
        // increment to count how many products have been registered
        function swap_positions($data1, $left, $right) 
        {
            $backup_old_data_right_value = $data1[$right];
            $data1[$right] = $data1[$left];
            $data1[$left] = $backup_old_data_right_value;
            return $data1;
        }
        for($i = 0; $i < $arr_length - 1; $i++) 
        {
            $min = $i;
            for($j = $i + 1; $j < $arr_length; $j++) 
            {
                if ($all_products[$j]->id < $all_products[$min]->id) 
                {
                    $min = $j;
                }
            }
            $all_products = swap_positions($all_products, $i, $min);
        }

        $sorted_products = $all_products;
        
        $arr_length = array_key_last($all_products);

        $id_to_insert = $sorted_products[$arr_length]->id + 1;
    }
    
    return $id_to_insert;
    // testestest.tech
?>