<?php

    include 'classes/Book.php';
    include 'classes/Dvd.php';
    include 'classes/Furniture.php';

    $books = new Book();
    $dvds = new Dvd();
    $furnitures = new Furniture();

    $arr_to_sort = array_merge($books->fetchAll(), $dvds->fetchAll(), $furnitures->fetchAll());
    $rows = [];

    // Implementing a selection sort to sort the  
    // array in order of the ID The Id are fix
    function swap_positions($data1, $left, $right) 
    {
        $backup_old_data_right_value = $data1[$right];
        $data1[$right] = $data1[$left];
        $data1[$left] = $backup_old_data_right_value;
        return $data1;
    }

    for($i = 0; $i < count($arr_to_sort) - 1; $i++) 
        {
            $min = $i;
            for($j = $i + 1; $j < count($arr_to_sort); $j++) 
            {
                if ($arr_to_sort[$j]->id < $arr_to_sort[$min]->id) 
                {
                    $min = $j;
                }
            }
            $arr_to_sort = swap_positions($arr_to_sort, $i, $min);
        }

    $rows = $arr_to_sort;
    $data = array('rows' => $rows);

    echo json_encode($data);

?>