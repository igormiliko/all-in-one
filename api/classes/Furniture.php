<?php

    // The goal of the polymorphism is to any subclass take your table name 
    // to apply the functions just in your instances;

    require_once 'Product.php';

    class Furniture extends Product
    {
        // All the functions in this class is abstract and take the polymorphism;
        public function insert($product_ID, $product_sku, $product_name, $product_price, $product_type, $product_property)
        {

        // Get the product Data   
            $ID = self::get_ID();                
            $SKU = self::get_sku();
            $NAME = self::get_name();
            $PRICE = self::get_price();
            $TYPE = self::get_type();
            $PROPERTY = self::get_property();

        // The PDO instance are private and visible just to the child objects;
        // to call the PDO we want to put DB;
            $sql = 'INSERT INTO furniture_table (id, product_sku, product_name, product_price, product_type, product_property) VALUES (:id, :sku, :named, :price, :typed, :property)';
            $stmt = DB::prepare($sql); 
            $stmt->bindParam(':id', $ID); 
            $stmt->bindParam(':sku', $SKU);
            $stmt->bindParam(':named', $NAME);
            $stmt->bindParam(':price', $PRICE);
            $stmt->bindParam(':typed', $TYPE);
            $stmt->bindParam(':property', $PROPERTY);
            return $stmt->execute(); 
            
        }

        public function fetchAll(){
          
        //Function to read the products from the dvd_table;
            $sql  = 'SELECT * FROM furniture_table';
            $stmt = DB::prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll();
                
        }

        public function delete($id_to_del)
        {
        
        //Function to del the products from the dvd_table;
            $sql = 'DELETE FROM furniture_table WHERE id = :deleting_product';

            $stmt = DB::prepare($sql);
            $stmt->bindParam(':deleting_product', $id_to_del, PDO::PARAM_INT);
            return $stmt->execute(); 
            
        }
    }
?>
