<?php

    require_once 'DB/Db.php';

    abstract class Product extends DB
    {
        
        // Datas in the table
        public $product_ID;
        public $product_sku;
        public $product_name;
        public $product_price;
        public $product_type;
        public $product_property;
        
        // Methods GETTERS AND SETTERS of the product
            
            // ID -> GET/SET
            public function get_ID () 
            {
                return $this->product_ID;
            }
            public function set_ID ($ID_setter) 
            {
                $this->product_ID = $ID_setter;
            }
            ///////////////////////////////////////////

            // SKU -> GET/SET
            public function get_sku () 
            {
                return $this->product_sku;
            }
            public function set_sku ($sku_setter) 
            {
                $this->product_sku = $sku_setter;
            }
            ///////////////////////////////////////////
            
            // NAME -> GET/SET
            public function get_name () 
            {
                return $this->product_name;
            }
            public function set_name ($name_setter) 
            {
                $this->product_name = $name_setter;
            }
            ///////////////////////////////////////////
            
            // PRICE -> GET/SET
            public function get_price () 
            {
                return $this->product_price;
            }
            public function set_price ($price_setter) 
            {
                $this->product_price = $price_setter;
            }
            ///////////////////////////////////////////

            // TYPE -> GET/SET
            public function get_type () 
            {
                return $this->product_type;
            }
            public function set_type ($type_setter) 
            {
                $this->product_type = $type_setter;
            }
            ///////////////////////////////////////////

            // PROPERTY -> GET/SET
            public function get_property () 
            {
                return $this->product_property;
            }
            public function set_property ($property_setter) 
            {
                $this->product_property = $property_setter;
            }
            ///////////////////////////////////////////

        /* Abstract function to make the polimorphysm in the insertion of the product 
           into database using the Object with Properties logic to MySQL, creating 
           different databases, on to one product type*/
        abstract public function insert($product_ID, $product_sku, $product_name, $product_price, $product_type, $product_property);

        // Fetch all products from database
        abstract public function fetchAll();

        // Delete products from database
        abstract public function delete($id_to_del);
    }
?>
