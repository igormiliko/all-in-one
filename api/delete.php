<?php
     include 'classes/Book.php';
     include 'classes/Dvd.php';
     include 'classes/Furniture.php';

    if(isset($_POST['products_to_delete']))
    {
        $deleted_all_items = 0;
        
        // Store the JSON notation (Array of objects)
        // and decoding this string
        $JSON_text = $_POST['products_to_delete'];
        $decodedText = html_entity_decode($JSON_text);

        // Decode 
        $products_to_del = json_decode($decodedText, true);

        $arr_length = sizeof($products_to_del);

        // loop to del all items in the products_to_delete
        // and optimized memory in comparsion of the last version :)
        // Thanks to show me my mistakes, I'm very thankfully and learn much
        // Blessed are you reading my code

        for ($i = 0; $i < $arr_length; $i++)
        {
        // Transforming the string of a array of objects in
        // a readable object to PHP
            $array_to_JSON = json_encode($products_to_del[$i]);
            $products_to_del[$i] = json_decode($array_to_JSON);

            $product_type_to_eval = $products_to_del[$i]->product_type;

            eval("\$product = new ".$product_type_to_eval."();");

            $product->delete($products_to_del[$i]->id);

            if($product->delete($products_to_del[$i]->id))
            {
                $deleted_all_items++;
            }
        }


        // A conditional to response success or error
        if($deleted_all_items == $arr_length)
        {
            $data = array('result' => 'success');
        }
        else{
            $data = array('result' => 'error');
        }

        echo json_encode($data);
    }
?>
