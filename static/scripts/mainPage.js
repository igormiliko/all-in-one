var application = new Vue({
    el: "#productApp",
    data:{    
        products: [],
        //Array of products to delete
        productsToDelete: [],
        
    },
    methods: {

        readProducts(){
            axios({
                url: '../../api/read.php',
                method: 'get'
            })
            .then(response => {
                console.log(response)
                this.products = response.data.rows
                console.log(this.products)
            })
            .catch(error => {
                console.log(error)
            })
        },

        deleteProducts(){
            var fd = new FormData();

            fd.append('products_to_delete', JSON.stringify(this.productsToDelete))

            axios({
                url: '../../api/delete.php',
                method: 'post',
                data: fd
            })
            .then(response => {
                console.log(response)
                application.readProducts()
            })
            .catch(error => {
                console.log(error)
            })
        },
    },
    computed: {
        //Styling Buttons
        btnAdd(){
            return "mr-4 py-1 px-2 rounded-md bg-green-400 hover:bg-green-300 text-white"
        },
        btnDel(){
            return "mr-4 py-1 px-2 rounded-md bg-red-400 hover:bg-red-300 text-white"
        },

        //Styling Product Cards
        productCards(){
            return "w-64 m-4 p-4 h-64 shadow-2xl rounded-lg bg-white"
        },

    },
    mounted: function() {
        this.readProducts()
    }
   
})