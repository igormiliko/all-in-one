Vue.component('input-blur', {
    props: ['value'],
    template: `<input v-on:blur="$emit('blur', this.onblur)"
                      v-bind:value="value"
                      v-on:input="$emit('input', $event.target.value)">`
  })


var application = new Vue({
    el: "#productApp",
    data:{

        // Datas to show the products attributes
            showDvd: false,
            showFurniture: false,
            showBook: false,
            showProductType: false,

        //Product Data
            SKU: '',
            productName: '',
            productType: '',
            toShowProduct: '',
            ruleProduct: ['Dvd', 'Furniture', 'Book'],
            productPrice: '',
            productSpeci: '',
            products: [],

        //Special data for Furniture
            furnitureWidth: '',
            furnitureHeight: '',
            furnitureLength: '',

            dvdSizeMB: '',
            bookWeight: '',

        //Array of products to delete
            productsToDelete: [],
        
    },
    methods: {
        onSubmit(){
            
            let equalSKU = true
            for(i = 0; i < this.products.length; i++){
                if(this.SKU == this.products[i].product_sku){
                    alert('!!! error ! The SKU must be diferent!')    
                    equalSKU = false    
                }
            }

            if(equalSKU == true && this.SKU !== '' && this.productName !== '' &&
            this.productPrice !== '' && this.productSpeci !== '') {
                
                var fd = new FormData()
                
                // Assingment  the values to your respective key
                fd.append('product_sku', this.SKU)
                fd.append('product_name', this.productName)
                fd.append('product_price', '$' + this.productPrice)
                fd.append('product_type', this.productType)
                fd.append('product_property', this.productSpeci)
                
                axios({
                    url: '../../api/create.php',
                    method: 'post',
                    data: fd
                })
                .then(response => {
                    console.log(response.data)
                    if(response.data.result == 'success') {
                    
                    // Default the values to initialize
                        this.SKU = ''
                        this.productName = ''
                        this.productPrice = ''
                        this.productSpeci = ''
                        this.furnitureWidth = ''
                        this.furnitureHeight = ''
                        this.furnitureLength = ''
                        this.dvdSizeMB = ''
                        this.bookWeight = ''
                        window.location.replace('../../index.html')
                    }
                    else {
                        console.log(response.data.result)
                        alert('!!! error ! record inserting problem !!!')
                    }
                })
                .catch(error => {
                    console.log(error)
                })
            } else {
                alert('Please, provide all data types!')
            }
        },
        readProducts(){
            axios({
                url: '../../api/read.php',
                method: 'get'
            })
            .then(response => {
                 console.log(response.data)
                this.products = response.data.rows
                console.log(this.products)
            })
            .catch(error => {
                console.log(error)
            })
        },

        cancelProduct(){

        // If you click in cancel the inputs are empty
            this.SKU = ''
            this.productName = ''
            this.productPrice = ''
            this.productSpeci = ''
            this.furnitureWidth = ''
            this.furnitureHeight = ''
            this.furnitureLength = ''

        },

        furnitureConcat() {
            this.productSpeci = `${this.furnitureWidth}cmX${this.furnitureHeight}cmX${this.furnitureLength}cm`
            console.log(this.productSpeci)
        },
    
    },
    computed: {
        //Styling Buttons
        btnAdd(){
            return "mr-4 py-1 px-2 rounded-md bg-green-400 hover:bg-green-300 text-white"
        },
        btnDel(){
            return "mr-4 py-1 px-2 rounded-md bg-red-400 hover:bg-red-300 text-white hover:pointer"
        },

        //Styling form inputs
        formInput(){
            return "sm:w-full lg:w-1/3 h-8 pb-1 px-2 rounded-md m-4 outline-none shadow"
        }
    },

    watch:{
        productType(e){
            let otherProductTypes = this.ruleProduct.filter((n) => n != e)
            
            let showProductType = `this.show${e} = true`
            let hideOtherProductType1 = `this.show${otherProductTypes[0]} = false`
            let hideOtherProductType2 = `this.show${otherProductTypes[1]} = false`

            this.ruleProduct.filter((n) => n != e)

            eval(showProductType)
            eval(hideOtherProductType1)
            eval(hideOtherProductType2)
        },

        dvdSizeMB(){
        // Format the data to concat the data with the 'MB'
            this.productSpeci = `${this.dvdSizeMB}MB`
        },

        // Format the data to concat the data with the 'MB'
        bookWeight(){
            this.productSpeci = `${this.bookWeight}KG`
        }
    },

    // To read the products when the page is loaded
    mounted() {
        this.readProducts()
    },        
})